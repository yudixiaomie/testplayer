#include <iostream>

using namespace std;
extern "C" {
#include <SDL2/SDL.h>
#include <libavformat/avformat.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>
}

#define REFRESH (SDL_USEREVENT+1)

int wake_up(void *data);

int exit_flag = -1;
int pause_flag = -1;

int main(int argc, char **argv) {
//    if (!SDL_Init(SDL_INIT_VIDEO))
//        cout << "Hello, World!" << endl;
//    else
//        cout << "init failed" << endl;

    //cout << avformat_configuration() << endl;

    // the file to open
    char *file_path = argv[1];//"girls.mp4";
    if (argc == 1) {
        cout << "need to append the video name like TestPlayer girls.mp4" << endl;
        return -1;
    }

    // register all components
    av_register_all();
    avformat_network_init();

    // allocate context
    AVFormatContext *pFormatCtx = avformat_alloc_context();
    !pFormatCtx ? (cout << "Can't get format context." << endl) : cout << "Get an format context." << endl;

    // open the input stream
    int ret = avformat_open_input(&pFormatCtx, file_path, NULL, NULL);
    ret == 0 ? cout << "open the input stream." << endl : cout << "Can't open the stream." << endl;

    // find the stream info
    ret = avformat_find_stream_info(pFormatCtx, NULL);
    ret >= 0 ? cout << "Find the stream info" << endl : cout << "Can't get the stream info" << endl;


    // get the video index
    int video_index = -1;
    for (int i = 0; i < pFormatCtx->nb_streams; i++) {
        if (pFormatCtx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
            video_index = i;
            break;
        }
    }
    video_index == -1 ? cout << "Can't get the video stream." << endl : cout << "Get the video stream." << endl;

    // get the parameters
    AVCodecParameters *pCodecParam = pFormatCtx->streams[video_index]->codecpar;
    pCodecParam ? cout << "Get the codec param." << endl : cout << "Can't get the codec param." << endl;

    // get the codec
    AVCodec *pCodec = avcodec_find_decoder(pCodecParam->codec_id);
    pCodec ? cout << "Get the codec ." << endl : cout << "Can't get the codec." << endl;

    // get the codec context
    AVCodecContext *pCodecCtx = avcodec_alloc_context3(pCodec);
    pCodecCtx ? cout << "Get the codec context." << endl : cout << "Can't get the codec context." << endl;
    // bugs
    pCodecCtx->extradata = pCodecParam->extradata;
    pCodecCtx->extradata_size = pCodecParam->extradata_size;

    // open the codec
    ret = avcodec_open2(pCodecCtx, pCodec, NULL);
    ret == 0 ? cout << "succeed on open codec." << endl : cout << "failed on open codec." << endl;

    // allocate a packet & frame
    AVPacket *packet = av_packet_alloc();
    AVFrame *frame = av_frame_alloc();
    AVFrame *frameYUV = av_frame_alloc();

    uint8_t *buffer = (uint8_t *) av_malloc(
            (size_t) av_image_get_buffer_size(AV_PIX_FMT_YUV420P, pCodecParam->width, pCodecParam->height, 1));
    av_image_fill_arrays(frameYUV->data, frameYUV->linesize, buffer, AV_PIX_FMT_YUV420P, pCodecParam->width,
                         pCodecParam->height, 1);

    SwsContext *img_convert_ctx = sws_getContext(pCodecParam->width, pCodecParam->height,
                                                 (AVPixelFormat) pCodecParam->format,
                                                 pCodecParam->width, pCodecParam->height, AV_PIX_FMT_YUV420P,
                                                 SWS_BICUBIC,
                                                 NULL, NULL, NULL);

    // get a window
    SDL_Window *sdl_window = SDL_CreateWindow(file_path, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                              pCodecParam->width, pCodecParam->height,
                                              SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

    // get a renderer
    SDL_Renderer *sdl_renderer = SDL_CreateRenderer(sdl_window, -1, SDL_RENDERER_TARGETTEXTURE);

    // get a texture
    SDL_Texture *sdl_texture = SDL_CreateTexture(sdl_renderer, SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING,
                                                 pCodecParam->width, pCodecParam->height);

    // get a rect
    SDL_Rect sdl_rect;

    // start a thread
    SDL_CreateThread(wake_up, NULL, 0);

    // get a sdl event
    SDL_Event sdl_event;


    int count = 0;

//    char out[] = "yuv.yuv";
//    FILE *out_file = fopen(out, "w");

    while (av_read_frame(pFormatCtx, packet) == 0) {
        if (packet->stream_index == video_index) {
            cout << "we get the stream data" << endl;
            // decode
            ret = avcodec_send_packet(pCodecCtx, packet);
            ret == 0 ? (ret = avcodec_receive_frame(pCodecCtx, frame)) : (ret = -1);
            // wait update call
            SDL_WaitEvent(&sdl_event);
//            SDL_PollEvent(&sdl_event);
            if (sdl_event.type == REFRESH) {
                if (ret == 0) {
                    // 去白边
                    sws_scale(img_convert_ctx, (const uint8_t *const *) frame->data, frame->linesize, 0,
                              pCodecParam->height,
                              frameYUV->data, frameYUV->linesize);

//                fwrite(frameYUV->data[0], 1, (size_t) pCodecParam->width * pCodecParam->height, out_file);
//                fwrite(frameYUV->data[1], 1, (size_t) pCodecParam->width * pCodecParam->height / 4, out_file);
//                fwrite(frameYUV->data[2], 1, (size_t) pCodecParam->width * pCodecParam->height / 4, out_file);
                    cout << "succeed on getting the picture." << ++count << endl;
                    SDL_UpdateTexture(sdl_texture, &sdl_rect, *frameYUV->data, frameYUV->linesize[0]);
                    sdl_rect.x = 0;
                    sdl_rect.y = 0;
                    sdl_rect.w = pCodecParam->width;
                    sdl_rect.h = pCodecParam->height;

                    SDL_RenderCopy(sdl_renderer, sdl_texture, NULL, &sdl_rect);
                    SDL_RenderPresent(sdl_renderer);
                }
            } else if (sdl_event.type == SDL_QUIT) {
                exit_flag = -1;
                break;
            } else if (sdl_event.type == SDL_KEYDOWN) {
                if (sdl_event.key.keysym.sym == SDLK_SPACE)
                    pause_flag = pause_flag == 0 ? -1 : 0;
            }
            cout << "type:   " << sdl_event.type << endl;
        }
        av_packet_unref(packet);
    }
    av_free(packet);
    av_frame_free(&frame);
    avcodec_close(pCodecCtx);
//    avcodec_free_context(&pCodecCtx);
    avformat_free_context(pFormatCtx);
    return 0;
}


int wake_up(void *data) {
    exit_flag = 0;
    while (exit_flag == 0) {
        if (exit_flag == -1)break;
        if (pause_flag == 0) continue;
        SDL_Event delay;
        delay.type = REFRESH;
        SDL_PushEvent(&delay);
        SDL_Delay(40);
    }

    SDL_Event quit;
    quit.type = SDL_QUIT;
    SDL_PushEvent(&quit);
}